# Spreadsheet comparison
LibreOffice workbook including a macro to compare two worksheets.

This workbook allows comparison of an Active and a Reference worksheet with respect to changes in row-based data sets.

## Data model
This macro is intended to analyse two tables saved as row-based data sets in two different sheets. The make-up of the tables to be compared needs to be identical, i.e. the order of the columns in both tables needs to be the same.
One column should contain data that can serve as a unique identifier ('Primary Key').

## Analysis
* Click on the button 'Compare Sheets' from the Quick Access toolbar at the top
* A dialog will be presented where the 'Active' and 'Reference' sheets as well as the column containing the 'Primary Key' can be selected. 
* The macro will then compare the two selected sheets row-by-row.

**New row in the active sheet compared to reference sheet**: 
If a primary key is found only in the active sheet, but not in the reference sheet, this row will be highlighted red in the active sheet.

**Data in the active sheet are modified as compared to the reference sheet**: 
If a primary key is found in both sheets, all columns of this row are compared and any column where a difference is detected is highlighted orange in the active sheet.

**Data have been removed from the active sheet as compared to reference sheet**: 
If a primary key is found only in the reference sheet, this row is copied to the end of the active sheet and highlighted grey.
